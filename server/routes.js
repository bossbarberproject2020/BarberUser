var bodyParser = Meteor.npmRequire('body-parser');

// Define our middleware using the Picker.middleware() method.
Picker.middleware(bodyParser.json());
Picker.middleware(bodyParser.urlencoded({ extended: false }));


Picker.route('/webhooks/inbound', (params, request, response, next) => {
  response.statusCode = 200;
  response.end('Received from Nexmo');
});

Picker.route('/webhooks/delivery_receipt', (params, request, response, next) => {
  if (request.method == "GET") {
    let body = params.query || {};
    handleWebhook(body);
  } else {
    let body = request.body || {};
    handleWebhook(body);
  }
  response.statusCode = 200;
  response.end('Received from Nexmo');
});

function handleWebhook(params) {
  console.log(params);
  if (params && params['status'] && params['messageId']) {
    //This is a DLR, check that your message has been delivered correctly
    if (params['status'] !== 'delivered') {
      SMSQueue.update({
        sent: true,
        'res.messages.0.message-id': params['messageId']
      }, {
        $set: {
          deliveredFail: params['status'] + ": " + params['err-code']
        }
      });
    } else {
      SMSQueue.remove({
        sent: true,
        'res.messages.0.message-id': params['messageId']
      });
    }
  }
}

