Meteor.methods({
  newAppointment: function (appointment, identify) {
    check(appointment, {
      barber_id: String,
      start_time: Date,
      end_time: Date,
      status: String,
      time: String,
      cus_name: String,
      cus_phone: String,
      noRemind: Boolean
    });

    if (appointment.start_time >= appointment.end_time) {
      throw new Meteor.Error('invalid-data',
        'Invalid Data!');
    }

    // var toDate = moment.utc().endOf('day');
    // var start_time = moment.utc(appointment.start_time);
    // if (start_time < moment.utc(toDate)) {

    var toDate = moment.utc(moment.tz(moment().toDate(), TIMEZONE).format("YYYY-MM-DD")).startOf('day'),
          start_time = moment.tz(appointment.start_time, TIMEZONE);
    console.log(toDate.format('YYYY-MM-DD'));
    console.log(start_time.format('YYYY-MM-DD'));
    if (start_time.format('YYYY-MM-DD') <= toDate.format('YYYY-MM-DD')) {
      throw new Meteor.Error('invalid-data',
        "One day advance booking required!");
    }

    start_time = moment.utc(appointment.start_time);

    if ((start_time._d.getDay() == 6 && start_time.get('hour') >= 17) || start_time._d.getDay() == 0 || start_time.get('hour') < 7) {
      throw new Meteor.Error('invalid-data',
        "Office doesn't work in this time!");
    }

    var existed_appointment = null;

    if (appointment.barber_id == "anyone") {
      var barbers_count = Barbers.find({}).count(),
          booked_number = Appointments.find({
            start_time: { $gte: appointment.start_time },
            start_time: { $lt: appointment.end_time },
            end_time: { $lte: appointment.end_time },
            end_time: { $gt: appointment.start_time },
            barber_id: 'anyone'
          }).count();

      if (barbers_count > 2) {
        existed_appointment = (booked_number >= 2);
      } else {
        existed_appointment = (booked_number >= barbers_count);
      }

      appointment.is_anyone = true;
    } else {
      var existed_barber = Barbers.findOne({_id: appointment.barber_id});
      if(!existed_barber) {
        throw new Meteor.Error('invalid-data',
          "The barber is not existed!");
      }

      existed_appointment = Appointments.findOne({
        barber_id: appointment.barber_id,
        start_time: { $gte: appointment.start_time },
        start_time: { $lt: appointment.end_time },
        end_time: { $lte: appointment.end_time },
        end_time: { $gt: appointment.start_time },
      });
    }

    if (existed_appointment) {
      throw new Meteor.Error(403, "This range hour has been ready booked!");
    }

    var appointmentID = Appointments.insert(appointment);

    Meteor.call('sendNotification', appointmentID, identify);

    return appointmentID;
  },
  verifyCode: function(code) {
    check(code, String);

    var setting_code = Settings.findOne({name: "code"});
    if(setting_code) {
      return setting_code.value == code;
    }

    return false;
  }
})
