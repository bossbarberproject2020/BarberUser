
Meteor.publish("barbers", function (date) {
  return Barbers.find({start_date: {$lte: date}});
});

Meteor.publish('images', function() {
  return Images.find({});
});

Meteor.publish("appointments", function (start_date, end_date) {
  return Appointments.find({
    $and: [
      {start_time: {$gte: moment.utc(start_date)._d}},
      {start_time: {$lt: moment.utc(end_date)._d}}
    ]
  });
});

Meteor.publish("booking_appointments", function (start_date, end_date) {
  return Appointments.find({
    $and: [
      {start_time: {$gte: moment.utc(start_date)._d}},
      {start_time: {$lt: moment.utc(end_date)._d}},
    ],
    status: {$in: ['booked', "completed"]}
  }, {
    sort: { type: 1, start_time: 1 }
  });
});

Meteor.publish('office_phone', function() {
  return Settings.find({
    name: 'phone'
  });
});