Fiber = Npm.require('fibers');
Future = Npm.require('fibers/future');
Agenda = Meteor.npmRequire('agenda');
var sleep = Meteor.npmRequire('sleep');

EasyNexmo = Meteor.npmRequire('easynexmo');
EasyNexmo.initialize('fb4ff735', '03120e8cdb033327', true);

AppDevices = new Mongo.Collection("appDevices");
SMSQueue = new Mongo.Collection("smsQueue");

SMSQueue._ensureIndex({ sent: 1 });
SMSQueue._ensureIndex({ sending: 1 });
SMSQueue._ensureIndex({ delayUntil: 1 });


sendSMS = new Agenda({ db: {
  address: process.env.MONGO_URL,
  collection: "sendSMS"
}});

sendSMS.define('send_remind_sms', function(job, done) {
  Fiber(function() {
    let now = +new Date();
    let messages = SMSQueue.find({ $and: [
        { sent: false },
        { sending: { $lt: now} },
        { $or: [
            { delayUntil: { $exists: false } },
            { delayUntil:  { $lte: new Date() } }
          ]
        },
      ]}).fetch();

    let msg = {};
    for (let i = 0; i < messages.length; i ++) {
      msg = messages[i];
      if (_.isEmpty(msg)) {
        continue;
      }

      if (i == 20) {
        // Sleep 1s each 20 messages
        sleep.sleep(1);
      }

      try {
        let now = +new Date(),
          timeoutAt = now + 60000,
          to = msg.to,
          message = msg.message;

        let reserved = SMSQueue.update({
          _id: msg._id,
          sent: false,
          sending: {$lte: now}
        }, {
          $set: {
            sending: timeoutAt
          }
        });

        if (reserved) {
          // Sleep 1s each message
          sleep.sleep(1);
          nexmoSendSMS(to, message, msg._id);
        }
      } catch(error) {
      }
    }

    done();
  }).run();
});

sendSMS.on('ready', function() {
  sendSMS.every('1 minute', 'send_remind_sms');
  sendSMS.start();
});

nexmoSendSMS = function(to, message, msgId) {
  var future = new Future();
  let err = {}, res = {};

  EasyNexmo.sendTextMessage('12012795541', to, message, {
    'type': 'text',
    'status-report-req': 1
  }, (err, res) => {
    Fiber(function() {
      SMSQueue.update({
        _id: msgId
      },{
        $set: {
          sent: true,
          sentAt: new Date(),
          sending: 0,
          success: !err,
          err: err,
          res: res,
        }
      });
    }).run();

    future.return({ err: err, res: res });
  });

  return future.wait();
}