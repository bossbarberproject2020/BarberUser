Push.debug = true;

Push.Configure({
  apn: {
    certData: Assets.getText('ios/apn-prd/barber_prd_cer.pem'),
    keyData: Assets.getText('ios/apn-prd/barber_prd_key.pem'),
    passphrase: "hopcode123!@#",
    production: true,
    gateway: "gateway.push.apple.com",
  },
  gcm: {
    apiKey: 'AIzaSyC9Kx8kjEhzbmIUSYZGp3qGdnDuVkEyRl0',
    projectNumber: 527439297046
  },
  // production: true,
  // 'sound' true,
  // 'badge' true,
  // 'alert' true,
  // 'vibrate' true,
  // 'sendInterval': 15000, Configurable interval between sending
  // 'sendBatchSize': 1, Configurable number of notifications to send per batch
  // 'keepNotifications': false,
  //
});


Push.allow({
  send: function(userId, notification) {
    return true; // Allow all users to send
  }
});


Meteor.methods({
  sendNotification: function(appointmentID, identify) {
    check(appointmentID, String);

    let appointment = Appointments.findOne({_id: appointmentID});
    if (!appointment) {
      return;
    }

    let barberName = appointment.barber_id;
    if (barberName != 'anyone') {
      let barber = Barbers.findOne({_id: appointment.barber_id});
      barberName = (barber && barber.name) || 'anyone';
    }
    let utcTime = moment.utc(appointment.start_time),
        bookTimeStr = utcTime.format("hh:mm A"),
        pushTime = moment(utcTime.subtract(1, 'hours').format().replace('+00:00', UTC_OFFSET))._d,
        message = 'You have an appointment with ' +  barberName + ' at '+ bookTimeStr + ' today!';

    if (identify) {
      let appDevice = AppDevices.findOne({identify: identify});
      if (appDevice && appDevice.token) {
        let pushId = Push.send({
          from: 'Barber',
          title: 'Barber booking remind',
          text: message,
          badge: 1,
          // query: {  },
          token: appDevice.token,
          delayUntil: pushTime
        });

        if (pushId) {
          Appointments.update({
            _id: appointmentID
          }, {
            $set: {
              pushId: pushId
            }
          });
        }
      }
    }

    if (appointment.cus_phone && !appointment.noRemind) {
      let smsId = SMSQueue.insert({
        message: "Men's Den App: " + message,
        sent: false,
        sending: 0,
        delayUntil: pushTime,
        to: appointment.cus_phone
      })

      if (smsId) {
        Appointments.update({
          _id: appointmentID
        }, {
          $set: {
            smsId: smsId
          }
        });
      }
    }
  },
  updateAppDevice: function(identify, token) {
    check(identify, String);
    AppDevices.update({
      identify: identify
    }, {
      $set: {
        token: token
      }
    }, {
      upsert: true
    });
  }
});

