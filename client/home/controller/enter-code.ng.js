angular.module("barber-user").controller("EnterCodeCtrl", ['$scope', '$meteor', '$state', '$location', '$ionicHistory', '$ionicPopup',
  function ($scope, $meteor, $state, $location, $ionicHistory, $ionicPopup) {

    $scope.submitCode = function(code){

      if (!Meteor.status().connected) {
        showAlert("Can't connect to server", "Please check your networking...");
        return;
      }

      $meteor.call('verifyCode', code).then(function(res){
        if(res) {
          $location.path( "/home" );
        }
        else {
          showAlert("Please try again", "The code is not valid!");
        }
      }, function(error){
        showAlert("Please try again", error.reason)
      })

    }

    $scope.viewPrivacy = function() {
      $state.go('privacy');
    }

    function showAlert(title, template) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: template,
        okType: 'button-positive button-clear'
      });
    }

  }]);
