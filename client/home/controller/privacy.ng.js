angular.module("barber-user").controller("PrivacyCtrl", ['$scope', '$meteor', '$state',
  function ($scope, $meteor, $state) {

    $scope.sendMail = function() {
      window.location.href = 'mailto:bbprivacy@hop-code.com';
    }

    $scope.goBack = function() {
      $state.go('enter-code');
    };

  }]);
