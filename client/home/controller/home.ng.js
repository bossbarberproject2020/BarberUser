angular.module("barber-user").controller("HomeCtrl", ['$scope', '$meteor', '$state', 
  function ($scope, $meteor, $state) {

    $scope.goBooking = function() {
      $state.go("booking");
    }

    $scope.goSchedule = function() {
      $state.go('schedule',{}, {reload: true});
    }

    $scope.callTel = function() {
      window.location.href = 'tel: '+ $scope.office_phone;
    }

    $scope.$meteorSubscribe('office_phone').then(function() {
      $scope.settings = $scope.$meteorCollection(function() {
        return Settings.find({
        });
      });

      $scope.office_phone = Settings.findOne({name: "phone"}).value;
    });

}]);
