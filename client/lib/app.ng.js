var barberMobile = angular
  .module('barber-user', [
    'angular-meteor',
    'ui.router',
    'ionic',
    'ionic-datepicker',
    'angularMoment',
    'ngIntlTelInput',
  ]);

if (Meteor.isCordova) {
  angular.element(document).on('deviceready', onReady);
} else {
  angular.element(document).ready(onReady);
}

function onReady() {
  angular.bootstrap(document, ['barber-user']);
}

barberMobile.config(function($ionicConfigProvider, ngIntlTelInputProvider) {
  // note that you can also chain configs
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.views.maxCache(1);
  ngIntlTelInputProvider.set({initialCountry: 'us'});
  ngIntlTelInputProvider.set({utilsScript: "/client/lib/intl-tel-input/utils.js"});
  ngIntlTelInputProvider.set({onlyCountries: ['us']});
});

barberMobile.run(function($ionicPopup) {
  Push.Configure({
    android: {
      senderID: 527439297046,
      alert: true,
      badge: true,
      sound: true,
      vibrate: true,
      clearNotifications: true,
      icon: 'pushicon',
      // iconColor: '#FF00FF'
    },
    ios: {
      alert: true,
      badge: true,
      sound: true
    }
  });

  Push.addListener('token', function(token) {
    // Token is { apn: 'xxxx' } or { gcm: 'xxxx' }
    let identify =  device.uuid + "_" + device.serial;
    Session.set('identify', identify);
    Meteor.call('updateAppDevice', identify, token, (err, result) => {  });
  });

  Push.addListener('error', function(err) {
    if (err.type == 'apn.cordova') {
      // console.log(err.error);
    }
  });

  Push.addListener('alert', function(notification) {
    // Called when message got a message in forground
  });

  Push.addListener('sound', function(notification) {
    // Called when message got a sound
  });

  Push.addListener('badge', function(notification) {
    // Called when message got a badge
  });

  Push.addListener('startup', function(notification) {
    // Called when message recieved on startup (cold+warm)
    $ionicPopup.alert({
      title: notification.title || 'Barber booking remind',
      template: notification.message,
      okType: 'button-positive button-clear'
    });
  });

  Push.addListener('message', function(notification) {
    $ionicPopup.alert({
      title: notification.title || 'Barber booking remind',
      template: notification.message,
      okType: 'button-positive button-clear'
    });
  });
})
