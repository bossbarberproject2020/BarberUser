angular
  .module('barber-user')
  .filter('calendar', calendar);

function calendar () {
  return function (time) {
    if (! time) return;

    return moment(time).format("MMMM D YYYY");
  }
}