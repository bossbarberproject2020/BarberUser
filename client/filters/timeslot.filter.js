angular
  .module('barber-user')
  .filter('timeSlotFilter', timeSlotFilter);

function timeSlotFilter () {
  return function (time_slot) {
    if (! time_slot) return;
    book_time = time_slot.split(" - ");
    start_time = book_time[0].split(":");
    end_time = book_time[1].split(":");

    // display_time = [moment.utc().hour(start_time[0]).minute(start_time[1]).format("hh:mm A"),
    //                 moment.utc().hour(end_time[0]).minute(end_time[1]).format("hh:mm A")]
    // return display_time.join(" - ");

    display_time = moment.utc().hour(start_time[0]).minute(start_time[1]).format("hh:mm A");
    return display_time;
  }
}

angular
  .module('barber-user')
  .filter('timeSlotWithDateFilter', timeSlotWithDateFilter);

function timeSlotWithDateFilter () {
  return function (time) {
    if (! time) return;

    return moment.utc(time).format("MMM DD YYYY hh:mm A");
  }
}

angular
  .module('barber-user')
  .filter('timeBookingFilter', timeBookingFilter);

function timeBookingFilter () {
  return function (time) {
    if (! time) return;

    return moment(time).format("hh:mm A");
  }
}

angular
  .module('barber-user')
  .filter('timeBookingUTCFilter', timeBookingUTCFilter);

function timeBookingUTCFilter () {
  return function (time) {
    if (! time) return;

    return moment.utc(time).format("hh:mm A");
  }
}