angular.module("barber-user").controller("ConfirmCtrl", ['$scope', 'currAppointment', '$state', '$ionicHistory', '$meteor', '$ionicPopup',
  function ($scope, currAppointment, $state, $ionicHistory, $meteor, $ionicPopup) {

  if(!currAppointment) {
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $state.go('booking');
  } else {
    $scope.newAppointment = currAppointment;
    if(currAppointment.barber_id == "anyone"){
      $scope.barber = {
        _id: "anyone",
        name: "Anyone"
      }
    }else{
      $scope.barber = $scope.$meteorObject(Barbers, currAppointment.barber_id, false);
    }

    $scope.customer = {
      noRemind: false
    };

    $scope.submitted = false;

    $scope.confirmBooking = function(isValid) {
      if(!isValid){
        $scope.submitted = true;
        return;
      }

      if (!Meteor.status().connected) {
        showAlert("Can't connect to server", "Please check your networking...");
        return;
      }

      $scope.newAppointment["cus_name"] = $scope.customer.name;
      $scope.newAppointment["cus_phone"] = $scope.customer.phone;
      $scope.newAppointment["noRemind"] = $scope.customer.noRemind || false;

      let identify = Session.get('identify');
      if (!identify && Meteor.isCordova) {
        identify =  device.uuid + "_" + device.serial;
        Session.set('identify', identify);
      }
      /*Create Booking*/
      $meteor.call('newAppointment', new_appointment, identify).then(function(){
        showAlert("Booked!", 'Appointment has been booked successfully. Thank you very much!');
      }, function(error){
        showAlert("Please try again", error.reason)
      })
    }

    $scope.goBack = function() {
      $state.go('booking');
    };
  }

  function showAlert(title, template) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: template,
      okType: 'button-positive button-clear'
    });

    alertPopup.then(function(res) {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('booking');
    });
  }

}]);