
angular.module("barber-user").config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('booking', {
      url: '/booking',
      templateUrl: 'client/booking/views/booking.ng.html',
      controller: 'BookingCtrl',
      controllerAs: 'bookctrl'
    })
    .state('schedule', {
      cache: false,
      url: '/schedule',
      templateUrl: 'client/schedule/views/schedule.ng.html',
      controller: 'ScheduleCtrl',
      controllerAs: 'schctrl'
    })
    .state('home', {
      url: '/home',
      templateUrl: 'client/home/views/index.ng.html',
      controller: 'HomeCtrl',
      controllerAs: 'homctrl'
    })
    .state('confirm', {
      url: '/confirm',
      params: {appointment: null},
      templateUrl: 'client/confirm/views/confirm.ng.html',
      controller: 'ConfirmCtrl',
      controllerAs: 'cfrmctrl',
      resolve: {
        currAppointment: function($stateParams) {
          return $stateParams.appointment;
        }
      }
    })
    .state('enter-code', {
        url: '/enter',
        templateUrl: 'client/home/views/enter-code.ng.html',
        controller: 'EnterCodeCtrl',
        controllerAs: 'entercodectrl'
    })
    .state('privacy', {
        url: '/privacy',
        templateUrl: 'client/home/views/privacy.ng.html',
        controller: 'PrivacyCtrl',
        controllerAs: 'privacyctrl'
    })
    .state('webhooks', {
      url: '/webhooks/*path',
      template: "<h1>Doesn't render, this is webhooks</h1>"
    });

  $urlRouterProvider.otherwise("/enter");
}).run(function($rootScope, $state, $timeout) {
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (toState.name == "webhooks") {
      event.preventDefault();
    }
  });
});;

