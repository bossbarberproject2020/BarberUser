angular.module("barber-user").controller("ScheduleCtrl", ['$scope', '$meteor', '$state', '$filter', '$ionicPopup', '$location', '$anchorScroll', '$ionicScrollDelegate',
  function ($scope, $meteor, $state, $filter, $ionicPopup, $location, $anchorScroll, $ionicScrollDelegate) {

  $scope.work_hours = WORK_HOURS;

  $scope.work_ranges = WORK_HOURS;

  setSelectedDate();

  $scope.datepickerObjectPopup = {
    titleLabel: 'Choose Date',
    todayLabel: 'Today',
    closeLabel: 'Close',
    setLabel: 'Set',
    errorMsgLabel : 'Please select time.',
    setButtonType : 'button-assertive',
    modalHeaderColor:'bar-positive',
    modalFooterColor:'bar-positive',
    templateType:'popup', //Optional
    mondayFirst: false, //Optional
    dateFormat: 'MMMM dd yyyy',
    callback: function (val) { //Optional
      setSelectedDate(val);
    }
  };


  $scope.$meteorAutorun(function() {
    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({start_date: {$lte: $scope.getReactively('currentEndDate')}})
      });
    });

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('booking_appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        }, {
          sort: { type: 1, start_time: 1 }
        });
      });
    });

  });

  $scope.goBack = function() {
    $state.go('home');
  };

  $scope.getBarberName = function(barber_id) {
    if (barber_id == 'anyone') {
      return '1st available';
    } else {
      var barber = $filter('filter')($scope.barbers, {_id: barber_id})[0];
      return barber.name;
    }
  }

  /* functions */

  function setSelectedDate(date) {
    if (typeof(date) === 'undefined') {
      date = moment().toDate();
    }
    $scope.today = date;

    $scope.currentDate = moment(date).format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
    $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
  }

}]);
