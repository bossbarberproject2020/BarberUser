angular.module("barber-user").controller("BookingCtrl", ['$scope', '$rootScope', '$meteor', '$state', '$filter', '$ionicPopup', '$location', '$anchorScroll', '$ionicScrollDelegate',
  function ($scope, $rootScope, $meteor, $state, $filter, $ionicPopup, $location, $anchorScroll, $ionicScrollDelegate) {

  $scope.images = $meteor.collectionFS(Images, false, Images).subscribe('images');
  $scope.work_hours = WORK_HOURS;

  $scope.work_ranges = WORK_HOURS;

  setSelectedDate();

  $scope.datepickerObjectPopup = {
    titleLabel: 'Choose Date',
    todayLabel: 'Today',
    closeLabel: 'Close',
    setLabel: 'Set',
    errorMsgLabel : 'Please select time.',
    setButtonType : 'button-assertive',
    modalHeaderColor:'bar-positive',
    modalFooterColor:'bar-positive',
    templateType:'popup', //Optional
    mondayFirst: false, //Optional
    dateFormat: 'MMMM dd yyyy',
    callback: function (val) { //Optional
      if (val) {
        setSelectedDate(val);
      }
    }
  };


  $scope.$meteorAutorun(function() {
    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({start_date: {$lte: $scope.getReactively('currentEndDate')}})
      });
    });

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        });
      });
    });

  });

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('barbers')) {
      $scope.new_row = ($scope.barbers.length % 3) === 0;
      $scope.has_anyone = $scope.barbers.length > 0;
      reLoadAppointment();
    }
  })

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('appointments')) {
      if($scope.getReactively("currentBarber")) {
        // when change currentBarber or when change appointments;
        reLoadAppointment();
      }
    }
  })

  $scope.bookAppointment = function (hour) {
    if(!$scope.currentBarber) {
      return;
    }

    if (!Meteor.status().connected) {
      showAlert("Can't connect to server", "Please check your networking or retry after a minute...");
      return;
    }

    book_time = hour.split(" - ");
    start_time = book_time[0].split(":");
    end_time = book_time[1].split(":");

    new_appointment =  {
      barber_id: $scope.currentBarber._id,
      start_time: moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d,
      end_time: moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d,
      status: "booked",
      time: hour
    };
    $state.go('confirm', {appointment: new_appointment});
  }

  $scope.selectBarber = function(barber) {
    if(barber == 'anyone') {
      $scope.currentBarber = {
        _id: 'anyone',
        name: 'Anyone',
        images: 'anyone'
      }
    }else{
      $scope.currentBarber = barber;
    }

    // reLoadAppointment();

    // $location.hash('calendar');
    // var handle = $ionicScrollDelegate.$getByHandle('content');
    // handle.anchorScroll('calendar');
  }

  $scope.getMainImage = function(images) {
    if (images && images.length && images[0] && images[0].id) {
      img = $filter('filter')($scope.images, {_id: images[0].id})[0];
      return (img ? img.url() : 'no-photo.png');
    } else {
      if(images == 'anyone'){
        return 'anyone-avatar.png'
      }else{
        return 'no-photo.png';
      }
    }
  };

  $scope.goBack = function() {
    $state.go('home');
  };

  /* functions */

  function setSelectedDate(date) {
    if (typeof(date) === 'undefined') {
      date = moment().toDate();
    }
    $scope.today = date;

    $scope.currentDate = moment(date).format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;

    let tmpEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    if (!moment(tmpEndDate).isSame($scope.currentEndDate)) {
      $scope.currentEndDate = tmpEndDate;
    }
  }

  function reLoadAppointment() {
    if (!$scope.currentBarber) {
      return;
    }

    $scope.currentCalendars = [];
    var get_day = $scope.today.getDay();
    for (var i = 0; i < $scope.work_hours.length; i++) {
      if (check_off(get_day, i)) {
        appointment = "off";
      } else{
        appointment = hasAppointment($scope.work_hours[i]);
      }
      $scope.currentCalendars.push({
        time: $scope.work_hours[i],
        appointment: appointment
      });
    };
  }

  function check_off(numDay, i) {
    switch(numDay) {
      case 0:
        return true;
      case 1:
      case 2:
      case 3:
      case 4:
        if(i < 2) {
          return true;
        }
        return false;
      case 5:
        return false;
      case 6:
        if (i >= WORK_HOURS.length - 2) {
          return true;
        }
        return false;
      default:
        return false
    }
  }

  function hasAppointment(hour) {
    var book_time = hour.split(" - ");
    var start_time = book_time[0].split(":");
    var end_time = book_time[1].split(":");
    p_start_time = moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d;
    p_end_time = moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d;


    if($scope.currentBarber._id == 'anyone'){
      booked_number = Appointments.find({
        start_time: { $gte: p_start_time},
        start_time: { $lt: p_end_time},
        end_time: { $lte: p_end_time},
        end_time: { $gt: p_start_time},
        barber_id: 'anyone'
      }).count();

      if ($scope.barbers.length > 2) {
        existed_appointment = (booked_number >= 2);
      } else {
        existed_appointment = (booked_number >= $scope.barbers.length);
      }

    }else{
      existed_appointment = Appointments.findOne({
        barber_id: $scope.currentBarber._id,
        start_time: { $gte: p_start_time},
        start_time: { $lt: p_end_time},
        end_time: { $lte: p_end_time},
        end_time: { $gt: p_start_time}
      })
    }

    return existed_appointment;
  }

  function showAlert(title, template) {

    $ionicPopup.alert({
      title: title,
      template: template,
      okType: 'button-positive button-clear'
    });
  }

}]);
