var createThumb = function(fileObj, readStream, writeStream) {
  gm(readStream, fileObj.name()).resize('100', '120').stream().pipe(writeStream);
}


Images = new FS.Collection("images", {
  stores: [
    new FS.Store.GridFS("thumbs", { transformWrite: createThumb }),
    new FS.Store.GridFS("original")
  ],
  filter: {
    allow: {
      contentTypes: ['image/*']
    }
  }
});

if (Meteor.isServer) {
  Images.allow({
    download: function () {
      return true;
    }
  });
}